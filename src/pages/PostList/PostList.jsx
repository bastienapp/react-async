import { useEffect, useState } from "react";
import { getAllPost } from "../../services/api";

function PostList() {
  const [postList, setPostList] = useState([]);

  useEffect(() => {
    // on fait quoi avec un promise ?
    // .then() et .catch()
    const allPostPromise = getAllPost();
    allPostPromise
      .then((postList) => setPostList(postList))
      .catch((error) => alert(error.message))
    // async / await
    // https://devtrium.com/posts/async-functions-useeffect
    /*
    async function callApi() {
      try {
        const allPost = await getAllPost();
        setPostList(allPost);
      } catch (error) {
        alert(error.message);
      }
    }
    callApi();
    */
  }, []);

  return (
    <ul>
      {postList.map((post) => (
        <li key={post.id}>{post.title}</li>
      ))}
    </ul>
  );
}

export default PostList;
