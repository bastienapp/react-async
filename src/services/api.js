import axios from "axios";

const API_URL = "https://jsonplaceholder.typicode.com"

export async function getAllPost() {
  try {
    const response = await axios
      .get(API_URL + "/posts")
    return response.data;
  } catch (error) {
    throw error
  }
}

export async function getPostById(postId) {
  try {
    const response = await axios
      .get(API_URL + "/posts/" + postId)
    return response.data;
  } catch (error) {
    throw error
  }
}